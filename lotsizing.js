function countDMA(out){
  let $ = require('jquery');
  const remote = require('electron').remote;
  const app = remote.app;
  var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
  var data= JSON.parse(sessionStorage.getItem('datapem'));
  var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
  var jeda =+sessionStorage.getItem('bulandma');;
  var ramal=[];
  var st=[];
  var sst=[];
  var at=[];
  var bt=[];
  var m=2;

  if(jeda==5){
    for(var x=0;x< bulan.length;x++){
      if(x==0){
        st[x]=0;
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        if(x<jeda-1){
        st[x]=0;
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
        }
        else{
          st[x]=0;
          var z=x;
          for(var y=jeda-1;y>=0;y--){
            st[x]+=parseInt(data[z]);
            z--;
          }
          st[x]=Math.round(st[x]/jeda);
         

          if(x<(jeda-1)*2){
            sst[x]=0;
            at[x]=0;
            bt[x]=0;
          }
          else{
            z=x;
            sst[x]=0;
            for(var y=4;y>=0;y--){
            sst[x]+=st[z];
            z--;
            }
            sst[x]=Math.round(sst[x]/jeda);

            at[x]=(2*st[x])-sst[x];
            bt[x]=Math.round((2/(jeda-1))*(st[x]-sst[x]));
           // app.console.log(at[x]+' '+bt[x]);
          }
        
        if(x>=(jeda*2)-1)ramal[x]=at[x-1]+bt[x-1];
        else ramal[x]=0;
        }
       
      }
    }
  }
  else if(jeda==3){
    for(var x=0;x<bulan.length;x++){
      if(x==0){
        st[x]=0;
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        if(x<jeda-1){
        st[x]=0;
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
        }
        else{
          st[x]=0;
          var z=x;
          for(var y=jeda-1;y>=0;y--){
            //app.console.log(data[z]);
            st[x]+=parseInt(data[z]);
            z--;
          }
          st[x]=Math.round(st[x]/jeda);

          if(x<=jeda){
            sst[x]=0;
            at[x]=0;
            bt[x]=0;
          }
          else{
            z=x;
            sst[x]=0;
            for(var y=jeda-1;y>=0;y--){
            sst[x]+=st[z];
            z--;
            }
            sst[x]=Math.round(sst[x]/jeda*10000)/10000;
            //app.console.log(sst[x]);
            at[x]=(2*st[x])-sst[x];
            bt[x]=Math.round((2*(st[x]-sst[x])/(jeda-1))*100)/100;
           //app.console.log(at[x]+' '+bt[x]);
          }
        
        if(x>jeda+1)ramal[x]=at[x-1]+bt[x-1];
        else ramal[x]=0;
        }
       
      }
      //app.console.log(bt[x]);
    }
  }
  else if(jeda==2){
    for(var x=0;x< bulan.length;x++){
      if(x==0){
        st[x]=0;
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        if(x<jeda){
        st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
        sst[x]=0;
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
        }
        else{
        st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
        sst[x]=Math.round((st[x]+st[x-1])/2);
        at[x]=(2*st[x])-sst[x];
        bt[x]=(2/(2-1))*(st[x]-sst[x]);
        if(x==jeda) ramal[x]=0;
        else ramal[x]=at[x-1]+bt[x-1];
        }
       
      }

    }
  }
    if(out==1)
        return at[bulan.length-1];
    else
        return bt[bulan.length-1];
}


function countDES(out){
  let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var des=+sessionStorage.getItem('alfades');
    var jeda =2;
    var ramal=[];
    var st=[];
    var sst=[];
    var at=[];
    var bt=[];
    var m=2;

  for(var x=0;x< bulan.length;x++){
    var peramalan;
    if(x==0){
      peramalan='';
      st[x]=parseInt(data[x]);
      sst[x]=parseInt(data[x]);
      at[x]=0;
      bt[x]=0;
      ramal[x]=0;
    }
    else{
      st[x]=Math.round((des*parseInt(data[x]))+((1-des)*st[x-1]));
      sst[x]=Math.round((des*st[x])+((1-des)*sst[x-1]));
      at[x]=(2*st[x])-sst[x];
      bt[x]=Math.round((des/(1-des))*(st[x]-sst[x]));
    
     if(x<jeda)
       ramal[x]=0;
     else
       ramal[x]=(at[x-1]+(bt[x-1]*m))*bahan;
    }
  }
  
  if(out==1)
        return at[bulan.length-1];
    else
        return bt[bulan.length-1];
}

function countLinier(){
  let $ = require('jquery');
  const remote = require('electron').remote;
  const app = remote.app;
  var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
  var data= JSON.parse(sessionStorage.getItem('datapem'));
  //var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
  //app.console.log(bahan);
  var jeda =0;
  var ramal=[];
 
  var t=0;
  var yt=0;
  var tyt=0;
  var t2=0;

  for(var x=0;x< bulan.length;x++){
    t+=(x+1);
    yt+=parseInt(data[x]);
    tyt+=(parseInt(data[x])*(x+1));
    t2+=(Math.pow((x+1),2));
  }

  
  var b=Math.round(((bulan.length*tyt)-(yt*t))/((bulan.length*t2)-Math.pow(t,2))*100)/100;
  var a=Math.round((yt-(b*t))/bulan.length);
  //app.console.log(b+' '+a);
  var y=bulan.length+1;
  for(var x=0;x<12;x++){
      //app.console.log(y);
    var peramalan=Math.round(a+(b*(y)));
    ramal[x]=peramalan;
    y++;
  }
  
  return ramal;
}

  function countDecom(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var jeda =0;
      var ramal=[];

      var ave=[];
      var ctr2=0; //untuk mendapatkan rata-ratanya
      ave[ctr2]=0;
      var hit=0;
      var start=0;
      var x=start;
     while(true){ // average simple
        if(data[x]==null)
          {
          ave[ctr2]=ave[ctr2]/hit;
          ctr2++;
          start++;
          hit=0;
          x=start;
          if(start==data.length-7) break;
          else ave[ctr2]=0;
          continue;
          }
        else{
          ave[ctr2]+=+data[x];
          
        }
        hit++;
        x++;
        if(hit==12){
          ave[ctr2]=ave[ctr2]/12;
          ctr2++;
          ave[ctr2]=0;
          start++;
          hit=0;
          x=start;
          if(start==data.length-4) break;
        }
      }


       var center=[];
       var si=[];
      ctr2=0; //untuk mendapatkan rata-ratanya
      center[ctr2]=0;
      hit=0;
      start=0;
      x=start;
      while(true){ // average centered
        if(ave[x]==null)
          {
          center[ctr2]=center[ctr2]/hit;
          si[ctr2]=+data[start+6]/center[ctr2];
          ctr2++;
          start++;
          hit=0;
          x=start;
          if(start==ave.length-1) break;
          else center[ctr2]=0;
          continue;
          }
        else{
          center[ctr2]+=ave[x];
        }
        hit++;
        x++;
        if(hit==6){
          center[ctr2]=center[ctr2]/6;
          si[ctr2]=+data[x]/center[ctr2];
          ctr2++;
          center[ctr2]=0;
          start++;
          hit=0;
          x=start;
        }
      }
      // app.console.log('');
      // app.console.log(si);

      var cek=[];
      var ctr=0;//untuk variabel bulan ke berapa
      var ctr3=0;//untuk bulannya
      hit=0;
      var unadjusted=[];
      var sumunad=0;
      unadjusted[ctr3]=0;
      x=0;
      y=x;
      while(true){
       
          if(si[x]!=null){
            unadjusted[ctr3]+=si[x];
            hit++;
            x+=12;
          }
          else{
            y++;
            x=y;
            unadjusted[ctr3]=unadjusted[ctr3]/hit;
            sumunad+=unadjusted[ctr3];
            hit=0;
            ctr3++;
            if(y==12) break;
            else unadjusted[ctr3]=0;
          }

      }

      x=6;
      var adjustedorder=[];
      ctr=0;
      for(var y=0;y<unadjusted.length;y++){
        adjustedorder[ctr]=unadjusted[x]*12/sumunad;
        ctr++;
        x++;
        if(x==12) x=0;
      }

      var deseason=[];
      x=0;
      for(var y=0;y<data.length;y++){
        deseason[y]=Math.round(+data[y]/adjustedorder[x]);
        x++;
        if(x==12){
          x=0;
        }
      }

       var t=0;
        var yt=0;
        var tyt=0;
        var t2=0;

        for(var x=0;x< deseason.length;x++){
          t+=(x+1);
          yt+=parseInt(deseason[x]);
          tyt+=(parseInt(deseason[x])*(x+1));
          t2+=(Math.pow((x+1),2));
        }

        var b=((bulan.length*tyt)-(yt*t))/((bulan.length*t2)-Math.pow(t,2));
        var a=(yt-(b*t))/bulan.length;
     
  hit=0;
  var trend=[];
    for(var x=0;x<bulan.length;x++){
      hit++;
      if(hit==12){
        hit=0;
      }
    }
    var g=bulan.length+1;
    for(var x=0;x<12;x++){
      var peramalan=Math.round(a+(b*g));
      trend[x]=peramalan;
      ramal[x]=Math.round(trend[x]*adjustedorder[hit]);
      hit++;
      g++;
      if(hit==12){
        hit=0;
      }
    }
    
    return ramal;
}

function eoq(param){
  let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      //var hari=JSON.parse(sessionStorage.getItem('hari'));
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else if(method==2)var ramal=countDecom();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=(i/100)*harga;
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var permintaan=[];
      
      var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=+ramal[x];
         totalpermintaan+=permintaan[x];
      }
      //app.console.log(totalramal);

      var eoq = Math.round(Math.sqrt((2*pesan*totalramal)/simpan));
      
      var change=stock;
      var jumlahpesan=0;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      var ctr=0;
      var ctr2=0;
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){
              
                }
                else{
                   
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==12){
                   
                    var cek=0;
                    
                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                   
                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
                    
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                        //app.console.log(y+' '+x);
                        jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                else{
                    
                    g++;
                    
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                        //app.console.log(y+' '+x);
                        jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==11){
                    
                    // ctr++;
                    
                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                    break;
                }
                
            }
        }
        
      }

      if(param==0){
        return 'EOQ';
      }
      else if(param==1){
        return Math.round(sumPoh*(simpan/48));
      }
      else if(param==2){
        return jumlahpesan*pesan;
      }
      else if(param==3){
        return sumPorec*harga;
      }
      else if(param==4){
        return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
      }
  
}

function veoq(param){
  let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      //var hari=JSON.parse(sessionStorage.getItem('hari'));
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else if(method==2)var ramal=countDecom();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=(i/100)*harga;
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var wacc=+sessionStorage.getItem('aman');
      var permintaan=[];
      
      var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=+ramal[x];
         totalpermintaan+=permintaan[x];
      }

     var eoq = Math.round(Math.sqrt((2*pesan*totalramal)/(harga*((wacc+i)/100))));
      
      var change=stock;
      var jumlahpesan=0;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      var ctr=0;
      var ctr2=0;
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){
              
                }
                else{
                   
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==12){
                   
                    var cek=0;
                    
                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                   
                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
                    
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                        //app.console.log(y+' '+x);
                        jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                else{
                    
                    g++;
                    
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                        //app.console.log(y+' '+x);
                        jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=eoq;
                        change=change+eoq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==11){
                    
                    // ctr++;
                    
                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                    break;
                }
                
            }
        }
        
      }

      if(param==0){
        return 'VBEOQ';
      }
      else if(param==1){
        return Math.round(sumPoh*(simpan/48));
      }
      else if(param==2){
        return jumlahpesan*pesan;
      }
      else if(param==3){
        return sumPorec*harga;
      }
      else if(param==4){
        return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
      }
  
}

function poq(param){

  let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var hari=JSON.parse(sessionStorage.getItem('hari'));
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else if(method==2)var ramal=countDecom();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=harga*(i/100);
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var pro=+sessionStorage.getItem('production');
      var max=+sessionStorage.getItem('max');
      var wacc=+sessionStorage.getItem('aman');
      var permintaan=[];
      //app.console.log(i+' '+wacc+' '+pro+' '+max);
      
      var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=ramal[x];
         totalpermintaan+=permintaan[x];
      }
      //app.console.log(totalramal);
      var rataramal=Math.round(totalramal/12);
      var ratapermintaan=Math.round(totalpermintaan/12);

      var pembagi =(i/100)*(wacc/100)*(1-(totalramal/max));
      var poq = Math.round(Math.sqrt((2*pro*totalramal)/pembagi));
      var jumlahpesan=0;
      var change=stock;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      var ctr=0;
      var ctr2=0;
     
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){
                }
                else{
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }

                }
                if(y==12){
                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
 
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                 
                }
                else{
                   
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==11){

                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                    break;
                }
                
            }
        }
        
      }

  if(param==0){
    return 'POQ';
  }
  else if(param==1){
    return Math.round(sumPoh*(simpan/48));
  }
  else if(param==2){
    return jumlahpesan*pesan;
  }
  else if(param==3){
    return sumPorec*harga;
  }
  else if(param==4){
    return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
  }
}

function vpoq(param){

  let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var hari=JSON.parse(sessionStorage.getItem('hari'));
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else if(method==2)var ramal=countDecom();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=harga*(i/100);
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var pro=+sessionStorage.getItem('production');
      var max=+sessionStorage.getItem('max');
      var tax=+sessionStorage.getItem('tax');
      var wacc=+sessionStorage.getItem('aman');
      var permintaan=[];
      //app.console.log(i+' '+wacc+' '+pro+' '+max);
      
      var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=ramal[x];
         totalpermintaan+=permintaan[x];
      }
      //app.console.log(totalramal);
      var rataramal=Math.round(totalramal/12);
      var ratapermintaan=Math.round(totalpermintaan/12);
      if(tax!=0)tax=tax/100;
      var pembagi =harga*(1-(totalramal/max))*((wacc/100)+((i/100)*(1-tax)));
      var poq = Math.round(Math.sqrt((2*pro*totalramal*(1-tax))/pembagi));

      var jumlahpesan=0;
      var change=stock;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      var ctr=0;
      var ctr2=0;
     
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){
                }
                else{
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }

                }
                if(y==12){
                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
 
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                 
                }
                else{
                   
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        sumPorec+=poq;
                        change=change+poq-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==11){

                    temp=[];
                    hit=0;
                    temp[hit]=-1;
                    break;
                }
                
            }
        }
        
      }

  if(param==0){
    return 'VBPOQ';
  }
  else if(param==1){
    return Math.round(sumPoh*(simpan/48));
  }
  else if(param==2){
    return jumlahpesan*pesan;
  }
  else if(param==3){
    return sumPorec*harga;
  }
  else if(param==4){
    return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
  }
}

function silver(param){
  let $ = require('jquery');
  const remote = require('electron').remote;
  const app = remote.app;
  var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
  var method=+sessionStorage.getItem('method');
  if(method==5) var ramal=countLinier();
  else if(method==4)var ramal=countDES();
  else if(method==2)var ramal=countDecom();
  else if(method==1)var ramal=countDMA();
  else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
  var harga=+sessionStorage.getItem('harga');
  var pesan=+sessionStorage.getItem('pesan');
  var i=+sessionStorage.getItem('i');
  var simpan=(i/100)*harga;
  var lead=+sessionStorage.getItem('lead');
  var safety=+sessionStorage.getItem('safety');
  var stock=+sessionStorage.getItem('stock');
  var permintaan=[];
  var simpanminggu=simpan/48;

   var totalramal=0;
  var totalkerja=0;
  var totalpermintaan=0;
  for(var x=0;x<12;x++){
     permintaan[x]=Math.round(+ramal[x]/4);
     totalramal+=+ramal[x];
     totalpermintaan+=permintaan[x];
     //app.console.log(permintaan[x]);
  }

 
  var ctr=0;
  var id=0;
  var ctr2=0;
  var change=stock;
  var start=0;
  for(var x=0;x<48;x++){
    if((permintaan[ctr]+safety-change)>0){
      break;
    }
    else start++;
    change=change-permintaan[ctr];
    ctr2++;
    if(ctr2==4){
      ctr++;
      ctr2=0;
    }
  }
  //app.console.log(ctr+' '+ctr2);
  var periode1=start+1; // start point
  var periode2=start+1;
  var periode3=0;
  var periode4=1;//untuk menghitung urutan dari 1 putaran periode yg ada
  var cumdemand=0;
  var partperiod=0;
  var cuminv=0;
  var totalcost=0;
  var costperiod=0;
  var totaldemand=0;
  var beforedemand=0;
  var beforecost=0;

  var silver=[];
  var save=0;
  for(var x=0;permintaan[ctr]!=null;x++){
    // var markup2='';
    // markup2+='<tr id="'+id+'">';
    // markup2+='<td>'+periode1+'</td>';  

    // markup2+='<td>'+permintaan[ctr]+'</td>';
    beforedemand=cumdemand;
    if(cumdemand==0)cumdemand=permintaan[ctr];
    else cumdemand=cumdemand+permintaan[ctr];
    // markup2+='<td>'+cumdemand+'</td>';
        
        if(periode3!=0){
          partperiod=(cumdemand*periode3)-totaldemand;
          periode3++;
        }
        else{
          periode3++;
        }
        // markup2+='<td>'+partperiod+'</td>';

         cuminv=Math.round(partperiod*simpanminggu);
      //  markup2+='<td>'+cuminv+'</td>';

        totalcost=pesan+cuminv;
        // markup2+='<td>'+totalcost+'</td>';
        
        beforecost=costperiod;
        costperiod=Math.round(totalcost/periode4);
        // markup2+='<td>'+costperiod+'</td>';
        // markup2+='</tr>';
        //$("#silver tbody").append(markup2);
        totaldemand+=cumdemand;
        periode1++;
        periode4++;
        if(beforecost<costperiod&&beforecost!=0){
          silver[save]=beforedemand;
          beforedemand=0;
          beforecost=0;
          periode3=0;
          periode4=1;
          cumdemand=0;
          partperiod=0;
          cuminv=0;
          totalcost=pesan;
          costperiod=pesan;
          totaldemand=0;
          save++;
          periode1--;
          // $("#"+(id-1)).css("background-color", "yellow");
        }
        else{
            ctr2++;
            if(ctr2==4){
              ctr++;
              ctr2=0;
            }
            if(permintaan[ctr]==null){
              silver[save]=cumdemand;
              save++;
            }
        }
       
        id++;
  }

  change=stock;
  var jumlahpesan=0;
  var sumPoh=0;
  var sumPorec=0;
  var temp=[];
  var hit=0;
  temp[hit]=-1;
  var g=1;
  ctr=0;
  ctr2=0;
  save=0;//untuk addstock
  save2=0;//untuk porec
  save3=0;//untuk porel
  for(var x=0;x<4;x++){
    $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
    if(x==0){
        for(var y=0;y<13;y++){
            if(y==0){
            }
            else{
                g++;
                if(change<safety||(permintaan[ctr2]+safety-change)>0){
                  jumlahpesan++;
                    temp[hit]=y-1;
                    hit++;
                    temp[hit]=-1;
                    var tambah;
                    if(silver[save]!=null){
                      tambah=silver[save];
                      save++;
                    }
                    sumPorec+=tambah;
                    change=change+tambah-permintaan[ctr2];
                    sumPoh+=change;
                }
                else{
                    change=change-permintaan[ctr2];
                    sumPoh+=change;
                }
                ctr++;
                if(ctr==4){
                    ctr2++;
                    ctr=0;
                }
                
            }
            if(y==12){

                break;
            }
        }
    }
    else{
        for(var y=0;y<12;y++){
            if(y==0){
                
                g++;
                if(change<safety||(permintaan[ctr2]+safety-change)>0){
                  jumlahpesan++;
                    temp[hit]=y;
                    hit++;
                    temp[hit]=-1;
                    var tambah;
                      if(silver[save]!=null){
                          tambah=silver[save];
                          save++;
                      }
                    sumPorec+=tambah;
                    change=change+tambah-permintaan[ctr2];
                    sumPoh+=change;
                }
                else{
                    change=change-permintaan[ctr2];
                    sumPoh+=change;
                }
                ctr++;
                if(ctr==4){
                    ctr2++;
                    ctr=0;
                }
                
            }
            else{
        
                if(change<safety||(permintaan[ctr2]+safety-change)>0){
                  jumlahpesan++;
                    temp[hit]=y;
                    hit++;
                    temp[hit]=-1;
                    var tambah;
                      if(silver[save]!=null){
                          tambah=silver[save];
                          save++;
                      }
                    sumPorec+=tambah;
                    change=change+tambah-permintaan[ctr2];
                    sumPoh+=change;
                }
                else{
                    change=change-permintaan[ctr2];
                    sumPoh+=change;
                }
                ctr++;
                if(ctr==4){
                    ctr2++;
                    ctr=0;
                }
               
            }
            if(y==11){
                
                break;
            }
            
        }
    }
    
  }
  //app.console.log(param);
  if(param==0){
    return 'Silver Meal';
  }
  else if(param==1){
    return Math.round(sumPoh*(simpan/48));
  }
  else if(param==2){
    return jumlahpesan*pesan;
  }
  else if(param==3){
    return sumPorec*harga;
  }
  else if(param==4){
    return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
  }
}

function ppb(param){
  var jumlahpesan=0;
  let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==2)var ramal=countDecom();
      else if(method==1)var ramal=countDMA();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=(i/100)*harga;
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var permintaan=[];
      var simpanminggu=simpan/48;

      var epp=Math.round(pesan/simpanminggu);
      //app.console.log(epp);

       var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=+ramal[x];
         totalpermintaan+=permintaan[x];
         //app.console.log(permintaan[x]);
      }

     
      var ctr=0;
      var id=0;
      var ctr2=0;
      var change=stock;
      var start=0;
      for(var x=0;x<48;x++){
        if((permintaan[ctr]+safety-change)>0){
          break;
        }
        else start++;
        change=change-permintaan[ctr];
        ctr2++;
        if(ctr2==4){
          ctr++;
          ctr2=0;
        }
      }
      //app.console.log(ctr+' '+ctr2);
      var periode1=start+1; // start point
      var periode2=start+1;
      var periode3=0;
      var periode4=1;//untuk menghitung urutan dari 1 putaran periode yg ada
      var cumdemand=0;
      var partperiod=0;
      var cuminv=0;
      var totalcost=0;
      var costperiod=0;
      var totaldemand=0;
      var beforedemand=0;
      var beforecost=0;

      var ppb=[];
      var save=0;
      for(var x=0;permintaan[ctr]!=null;x++){
        // var markup2='';
        // markup2+='<tr id="'+id+'">';
        // markup2+='<td>'+periode1+'</td>';  

        // markup2+='<td>'+permintaan[ctr]+'</td>';
        beforedemand=cumdemand;
        if(cumdemand==0)cumdemand=permintaan[ctr];
        else cumdemand=cumdemand+permintaan[ctr];
        // markup2+='<td>'+cumdemand+'</td>';
            
            if(periode3!=0){
              partperiod=(cumdemand*periode3)-totaldemand;
              periode3++;
            }
            else{
              periode3++;
            }
            // markup2+='<td>'+partperiod+'</td>';

            // markup2+='</tr>';
            // $("#ppb tbody").append(markup2);
            totaldemand+=cumdemand;
            periode1++;
            periode4++;
            if(partperiod>epp){
              ppb[save]=beforedemand;
              beforedemand=0;
              beforecost=0;
              periode3=0;
              periode4=1;
              cumdemand=0;
              partperiod=0;
              cuminv=0;
              totalcost=pesan;
              costperiod=pesan;
              totaldemand=0;
              save++;
              periode1--;
              // $("#"+(id-1)).css("background-color", "yellow");
            }
            else{
                ctr2++;
                if(ctr2==4){
                  ctr++;
                  ctr2=0;
                }
                if(permintaan[ctr]==null){
                  ppb[save]=cumdemand;
                  save++;
                }
            }
           
            id++;
      }

      change=stock;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      ctr=0;
      ctr2=0;
      save=0;//untuk addstock
      save2=0;//untuk porec
      save3=0;//untuk porel
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){

                }
                else{
                    
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                        if(ppb[save]!=null){
                          tambah=ppb[save];
                          save++;
                        }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                  
                }
                if(y==12){

                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
                   
                    g++;
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                          if(ppb[save]!=null){
                              tambah=ppb[save];
                              save++;
                          }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                else{
                  
                    g++;
                   
                    if(change<safety||(permintaan[ctr2]+safety-change)>0){
                      jumlahpesan++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                          if(ppb[save]!=null){
                              tambah=ppb[save];
                              save++;
                          }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==11){
                    break;
                }
                
            }
        }
        
      }

  if(param==0){
    return 'PPB';
  }
  else if(param==1){
    return Math.round(sumPoh*(simpan/48));
  }
  else if(param==2){
    return jumlahpesan*pesan;
  }
  else if(param==3){
    return sumPorec*harga;
  }
  else if(param==4){
    return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
  }
}

function ltc(param){
var jumlahpesan=0;

let $ = require('jquery');
const remote = require('electron').remote;
const app = remote.app;
var method=+sessionStorage.getItem('method');
if(method==5) var ramal=countLinier();
else if(method==4)var ramal=countDES();
else if(method==2)var ramal=countDecom();
else if(method==1)var ramal=countDMA();
else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
var harga=+sessionStorage.getItem('harga');
var pesan=+sessionStorage.getItem('pesan');
var i=+sessionStorage.getItem('i');
var simpan=(i/100)*harga;
var lead=+sessionStorage.getItem('lead');
var safety=+sessionStorage.getItem('safety');
var stock=+sessionStorage.getItem('stock');
var permintaan=[];
var simpanminggu=simpan/48;

var epp=Math.round(pesan/simpanminggu);
//app.console.log(epp);

 var totalramal=0;
var totalkerja=0;
var totalpermintaan=0;
for(var x=0;x<12;x++){
   permintaan[x]=Math.round(+ramal[x]/4);
   totalramal+=+ramal[x];
   totalpermintaan+=permintaan[x];
   //app.console.log(permintaan[x]);
}


var ctr=0;
var id=0;
var ctr2=0;
var change=stock;
var start=0;
for(var x=0;x<48;x++){
  if((permintaan[ctr]+safety-change)>0){
    break;
  }
  else start++;
  change=change-permintaan[ctr];
  ctr2++;
  if(ctr2==4){
    ctr++;
    ctr2=0;
  }
}
//app.console.log(ctr+' '+ctr2);
var periode1=start+1; // start point
var periode2=start+1;
var periode3=0;
var periode4=1;//untuk menghitung urutan dari 1 putaran periode yg ada
var cumdemand=0;
var partperiod=0;
var cuminv=0;
var totalcost=0;
var costperiod=0;
var totaldemand=0;
var beforedemand=0;
var beforecost=0;

var ltc=[];
var save=0;
for(var x=0;permintaan[ctr]!=null;x++){
  // var markup2='';
  // markup2+='<tr id="'+id+'">';
  // markup2+='<td>'+periode1+'</td>';  

  // markup2+='<td>'+permintaan[ctr]+'</td>';
  beforedemand=cumdemand;
  if(cumdemand==0)cumdemand=permintaan[ctr];
  else cumdemand=cumdemand+permintaan[ctr];
  // markup2+='<td>'+cumdemand+'</td>';
      
      if(periode3!=0){
        partperiod=Math.round(((cumdemand*periode3)-totaldemand)*simpanminggu);
        periode3++;
      }
      else{
        periode3++;
      }

  
      totaldemand+=cumdemand;
      periode1++;
      periode4++;
      if(partperiod>epp){
        ltc[save]=beforedemand;
        beforedemand=0;
        beforecost=0;
        periode3=0;
        periode4=1;
        cumdemand=0;
        partperiod=0;
        cuminv=0;
        totalcost=pesan;
        costperiod=pesan;
        totaldemand=0;
        save++;
        periode1--;
        // $("#"+(id-1)).css("background-color", "yellow");
      }
      else{
          ctr2++;
          if(ctr2==4){
            ctr++;
            ctr2=0;
          }
          if(permintaan[ctr]==null){
            ltc[save]=cumdemand;
            save++;
          }
      }
     
      id++;
}

change=stock;
var sumPoh=0;
var sumPorec=0;
var temp=[];
var hit=0;
temp[hit]=-1;
var g=1;
ctr=0;
ctr2=0;
save=0;//untuk addstock
save2=0;//untuk porec
save3=0;//untuk porel
for(var x=0;x<4;x++){
  $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
  if(x==0){
      for(var y=0;y<13;y++){
          if(y==0){
          }
          else{

              g++;
              if(change<safety||(permintaan[ctr2]+safety-change)>0){
                jumlahpesan++;
                  temp[hit]=y-1;
                  hit++;
                  temp[hit]=-1;
                  var tambah;
                  if(ltc[save]!=null){
                    tambah=ltc[save];
                    save++;
                  }
                  sumPorec+=tambah;
                  change=change+tambah-permintaan[ctr2];
                  sumPoh+=change;
              }
              else{
                  change=change-permintaan[ctr2];
                  sumPoh+=change;
              }
              ctr++;
              if(ctr==4){
                  ctr2++;
                  ctr=0;
              }
           
          }
          if(y==12){

              break;
          }
      }
  }
  else{
      for(var y=0;y<12;y++){
          if(y==0){
              g++;
              if(change<safety||(permintaan[ctr2]+safety-change)>0){
                jumlahpesan++;
                  temp[hit]=y;
                  hit++;
                  temp[hit]=-1;
                  var tambah;
                    if(ltc[save]!=null){
                        tambah=ltc[save];
                        save++;
                    }
                  sumPorec+=tambah;
                  change=change+tambah-permintaan[ctr2];
                  sumPoh+=change;
              }
              else{
                  change=change-permintaan[ctr2];
                  sumPoh+=change;
              }
              ctr++;
              if(ctr==4){
                  ctr2++;
                  ctr=0;
              }
          
          }
          else{
              g++;
    
              if(change<safety||(permintaan[ctr2]+safety-change)>0){
                jumlahpesan++;
                  temp[hit]=y;
                  hit++;
                  temp[hit]=-1;
                  var tambah;
                    if(ltc[save]!=null){
                        tambah=ltc[save];
                        save++;
                    }
                  sumPorec+=tambah;
                  change=change+tambah-permintaan[ctr2];
                  sumPoh+=change;
              }
              else{
                  change=change-permintaan[ctr2];
                  sumPoh+=change;
              }
              ctr++;
              if(ctr==4){
                  ctr2++;
                  ctr=0;
              }
            
          }
          if(y==11){
              break;
          }
          
      }
  }
  
}

if(param==0){
  return 'LTC';
}
else if(param==1){
  return Math.round(sumPoh*(simpan/48));
}
else if(param==2){
  return jumlahpesan*pesan;
}
else if(param==3){
  return sumPorec*harga;
}
else if(param==4){
  return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
}
}

function luc(param){
var jumlahpesan=0;

let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==2)var ramal=countDecom();
      else if(method==1)var ramal=countDMA();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var harga=+sessionStorage.getItem('harga');
      var pesan=+sessionStorage.getItem('pesan');
      var i=+sessionStorage.getItem('i');
      var simpan=(i/100)*harga;
      var lead=+sessionStorage.getItem('lead');
      var safety=+sessionStorage.getItem('safety');
      var stock=+sessionStorage.getItem('stock');
      var permintaan=[];
      var simpanminggu=simpan/48;

      var epp=Math.round(pesan/simpanminggu);
      //app.console.log(epp);

       var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<12;x++){
         permintaan[x]=Math.round(+ramal[x]/4);
         totalramal+=+ramal[x];
         totalpermintaan+=permintaan[x];
         //app.console.log(permintaan[x]);
      }

     
      var ctr=0;
      var id=0;
      var ctr2=0;
      var change=stock;
      var start=0;
      for(var x=0;x<48;x++){
        if((permintaan[ctr]+safety-change)>0){
          break;
        }
        else start++;
        change=change-permintaan[ctr];
        ctr2++;
        if(ctr2==4){
          ctr++;
          ctr2=0;
        }
      }
      //app.console.log(ctr+' '+ctr2);
      var periode1=start+1; // start point
      var periode2=start+1;
      var periode3=0;
      var periode4=1;//untuk menghitung urutan dari 1 putaran periode yg ada
      var cumdemand=0;
      var partperiod=0;
      var holdcost=0;
      var totalcost=0;
      var unitcost=0;
      var totaldemand=0;
      var beforedemand=0;
      var beforecost=0;

      var luc=[];
      var luc2=[];
      var save=0;
      for(var x=0;permintaan[ctr]!=null;x++){
        // var markup2='';
        // markup2+='<tr id="'+id+'">';
        // markup2+='<td>'+periode1+'</td>';  

        // markup2+='<td>'+permintaan[ctr]+'</td>';
        beforedemand=cumdemand;
        if(cumdemand==0)cumdemand=permintaan[ctr];
        else cumdemand=cumdemand+permintaan[ctr];
        // markup2+='<td>'+cumdemand+'</td>';
            
            if(periode3!=0){
              partperiod=Math.round(((cumdemand*periode3)-totaldemand)*simpanminggu);
              periode3++;
            }
            else{
              periode3++;
            }
            // markup2+='<td>'+partperiod+'</td>';

             //holdcost=Math.round(partperiod*simpanminggu);
             holdcost=pesan+partperiod;
          
            
            beforecost=unitcost;
            unitcost=(holdcost/cumdemand);
           
            totaldemand+=cumdemand;
            periode1++;
            periode4++;
            if(beforecost<unitcost&&beforecost!=0){
              luc[save]=beforedemand;
              luc2[save]=periode1-1;
              //app.console.log(luc2[save]);
              beforedemand=0;
              beforecost=0;
              periode3=0;
              periode4=1;
              cumdemand=0;
              partperiod=0;
              holdcost=0;
              totalcost=pesan;
              unitcost=pesan;
              totaldemand=0;
              save++;
              periode1--;
              // $("#"+(id-1)).css("background-color", "yellow");
            }
            else{
                ctr2++;
                if(ctr2==4){
                  ctr++;
                  ctr2=0;
                }
                if(permintaan[ctr]==null){
                  luc[save]=cumdemand;
                  save++;
                }
            }
           
            id++;
      }

      change=stock;
      var sumPoh=0;
      var sumPorec=0;
      var temp=[];
      var hit=0;
      temp[hit]=-1;
      var g=1;
      ctr=0;
      ctr2=0;
      var ctr3=0; //untuk lucnya
      save=0;//untuk addstock
      save2=0;//untuk porec
      save3=0;//untuk porel
      for(var x=0;x<4;x++){
        $('#mrp').append('<table border="1" style="font-weight: bold;" class="table table-hover" id="tabel'+x+'"><tr></tr></table><br><br>');
        if(x==0){
            for(var y=0;y<13;y++){
                if(y==0){
                }
                else{
                    
                    g++;
                    
                    if(change<safety||(permintaan[ctr2]+safety-change)>0||luc2[ctr3]==g-1){
                        jumlahpesan++;
                        if(g-1!=start+1)ctr3++;
                        temp[hit]=y-1;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                        if(luc[save]!=null){
                          tambah=luc[save];
                          save++;
                        }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                    
                }
                if(y==12){

                    break;
                }
            }
        }
        else{
            for(var y=0;y<12;y++){
                if(y==0){
                    g++;
                    
                    if(change<safety||(permintaan[ctr2]+safety-change)>0||luc2[ctr3]==g-1){
                        jumlahpesan++;
                        if(g-1!=start+1)ctr3++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                        if(luc[save]!=null){
                              tambah=luc[save];
                              save++;
                        }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                }
                else{
                    g++;
                    
                    if(change<safety||(permintaan[ctr2]+safety-change)>0||luc2[ctr3]==g-1){
                        jumlahpesan++;
                        if(g-1!=start+1)ctr3++;
                        temp[hit]=y;
                        hit++;
                        temp[hit]=-1;
                        var tambah;
                          if(luc[save]!=null){
                              tambah=luc[save];
                              save++;
                          }
                        sumPorec+=tambah;
                        change=change+tambah-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    else{
                        change=change-permintaan[ctr2];
                        sumPoh+=change;
                    }
                    ctr++;
                    if(ctr==4){
                        ctr2++;
                        ctr=0;
                    }
                   
                }
                if(y==11){
                   
                    break;
                }
                
            }
        }
        
      }

if(param==0){
  return 'LUC';
}
else if(param==1){
  return Math.round(sumPoh*(simpan/48));
}
else if(param==2){
  return jumlahpesan*pesan;
}
else if(param==3){
  return sumPorec*harga;
}
else if(param==4){
  return (sumPorec*harga)+(jumlahpesan*pesan)+(Math.round(sumPoh*(simpan/48)));
}
}
