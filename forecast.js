function countSMA(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      //var jeda =+sessionStorage.getItem('bulansma');
      var jeda=+sessionStorage.getItem('bulansma');
      var ramal=[];

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x<jeda){
        peramalan='';
        ramal[x]=0;
      }
      else{
        peramalan=0;
        for(var y=x-jeda;y<bulan.length;y++){
          if(y<x)peramalan+=parseInt(data[y]);
          else if(y>x) break;
        }
       peramalan=peramalan/jeda;
       ramal[x]=Math.round(peramalan);
      }
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
      }
    }

    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-jeda))*100)/100;

    sessionStorage.setItem('meSMA',ME);
    sessionStorage.setItem('maeSMA',MAE);
    sessionStorage.setItem('mseSMA',MSE);
    sessionStorage.setItem('mapeSMA',MAPE);
    return ramal;
  }

  function countDMA(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var jeda =+sessionStorage.getItem('bulandma');
    var ramal=[];
    var st=[];
    var sst=[];
    var at=[];
    var bt=[];

    if(jeda==5){
      for(var x=0;x< bulan.length;x++){
        if(x==0){
          st[x]=0;
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
        }
        else{
          if(x<jeda-1){
          st[x]=0;
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
          }
          else{
            st[x]=0;
            var z=x;
            for(var y=jeda-1;y>=0;y--){
              st[x]+=parseInt(data[z]);
              z--;
            }
            st[x]=Math.round(st[x]/jeda);
           
  
            if(x<(jeda-1)*2){
              sst[x]=0;
              at[x]=0;
              bt[x]=0;
            }
            else{
              z=x;
              sst[x]=0;
              for(var y=4;y>=0;y--){
              sst[x]+=st[z];
              z--;
              }
              sst[x]=Math.round(sst[x]/jeda);
  
              at[x]=(2*st[x])-sst[x];
              bt[x]=Math.round((2/(jeda-1))*(st[x]-sst[x]));
             // app.console.log(at[x]+' '+bt[x]);
            }
          
          if(x>=(jeda*2)-1)ramal[x]=at[x-1]+bt[x-1];
          else ramal[x]=0;
          }
         
        }

      }
    }
    else if(jeda==3){
      for(var x=0;x< bulan.length;x++){
        if(x==0){
          st[x]=0;
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
        }
        else{
          if(x<jeda-1){
          st[x]=0;
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
          }
          else{
            st[x]=0;
            var z=x;
            for(var y=jeda-1;y>=0;y--){
              st[x]+=parseInt(data[z]);
              z--;
            }
            st[x]=Math.round(st[x]/jeda);
            
  
            if(x<=jeda){
              sst[x]=0;
              at[x]=0;
              bt[x]=0;
            }
            else{
              z=x;
              sst[x]=0;
              for(var y=jeda-1;y>=0;y--){
              sst[x]+=st[z];
              z--;
              }
              sst[x]=Math.round(sst[x]/jeda*10000)/10000;
              //app.console.log(sst[x]);
              at[x]=(2*st[x])-sst[x];
              bt[x]=Math.round((2*(st[x]-sst[x])/(jeda-1))*10000)/10000;
              app.console.log(at[x]+' '+bt[x]);
            }
          
          if(x>jeda+1)ramal[x]=at[x-1]+bt[x-1];
          else ramal[x]=0;
          }
         
        }
        
      }
    }
    else if(jeda==2){
      for(var x=0;x< bulan.length;x++){
        if(x==0){
          st[x]=0;
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
        }
        else{
          if(x<jeda){
          st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
          sst[x]=0;
          at[x]=0;
          bt[x]=0;
          ramal[x]=0;
          }
          else{
          st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
          sst[x]=Math.round((st[x]+st[x-1])/2);
          at[x]=(2*st[x])-sst[x];
          bt[x]=(2/(2-1))*(st[x]-sst[x]);
          if(x==jeda) ramal[x]=0;
          else ramal[x]=at[x-1]+bt[x-1];
          }
         
        }
       
      }
    }
      
  
  var xi=0;
  var fi=0;
  var kesalahan=0;
  var kesalahanA=0;
  var kesalahanK=0;
  var kesalahanP=0;
  var kesalahanPA=0;
  for(var x=0;x< bulan.length;x++){
    if(x<=jeda){
      continue;
    }
    else{
      if(ramal[x]==0)continue;
      xi+=parseInt(data[x]);
      fi+=ramal[x];
      kesalahan+=(parseInt(data[x])-ramal[x]);
      //app.console.log(parseInt(data[x])+' - '+ramal[x]+' = '+(data[x]-ramal[x])+' '+Math.pow(data[x]-ramal[x],2));
      kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
      kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
      kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
      kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100); 
    }
  }
  
  if(jeda==5){
  var ME=Math.round((Math.abs(kesalahan)/(bulan.length-((jeda*2)+1)))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-((jeda*2)+1)))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-((jeda*2)+1)))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-((jeda*2)+1)))*100)/100;
  }
  else if(jeda==3){
    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-((jeda-1)*2+1)))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-((jeda-1)*2+1)))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-((jeda-1)*2+1)))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-((jeda-1)*2+1)))*100)/100;
  }
  else if(jeda==2){
    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-(jeda+1)))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-(jeda+1)))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-(jeda+1)))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-(jeda+1)))*100)/100;
  }
  
    sessionStorage.setItem('meDMA',ME);
    sessionStorage.setItem('maeDMA',MAE);
    sessionStorage.setItem('mseDMA',MSE);
    sessionStorage.setItem('mapeDMA',MAPE);
    return ramal;
  }

  function countSES(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var ses =+sessionStorage.getItem('alfases');
    var jeda=1;
    var ramal=[];

  for(var x=0;x< bulan.length;x++){
    var peramalan;
    if(x<jeda){
      peramalan='';
      ramal[x]=0;
    }
    else if(x==jeda){
      peramalan=parseInt(data[x-1]);
      ramal[x]=parseInt(data[x-1]);
    }
    else{
      peramalan=0;
      peramalan=(ses*parseInt(data[x-1]))+((1-ses)*ramal[x-1]);
      ramal[x]=Math.round(peramalan);
    }

  }
  
  var xi=0;
  var fi=0;
  var kesalahan=0;
  var kesalahanA=0;
  var kesalahanK=0;
  var kesalahanP=0;
  var kesalahanPA=0;
  for(var x=0;x< bulan.length;x++){
    if(x<jeda){
      continue;
    }
    else{
      xi+=parseInt(data[x]);
      fi+=ramal[x];
      kesalahan+=(parseInt(data[x])-ramal[x]);
      kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
      kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
      kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
      kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
      
    }
  }
  
  var ME=Math.round((Math.abs(kesalahan)/(bulan.length-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-jeda))*100)/100;

    sessionStorage.setItem('meSES',ME);
    sessionStorage.setItem('maeSES',MAE);
    sessionStorage.setItem('mseSES',MSE);
    sessionStorage.setItem('mapeSES',MAPE);
    return ramal;
}

function countDES(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var des=+sessionStorage.getItem('alfades');
      var jeda =2;
      var ramal=[];
      var st=[];
      var sst=[];
      var at=[];
      var bt=[];

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x==0){
        peramalan='';
        st[x]=parseInt(data[x]);
        sst[x]=parseInt(data[x]);
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        st[x]=Math.round((des*parseInt(data[x]))+((1-des)*st[x-1]));
        sst[x]=Math.round((des*st[x])+((1-des)*sst[x-1]));
        at[x]=(2*st[x])-sst[x];
        bt[x]=Math.round((des/(1-des))*(st[x]-sst[x]));
      
       if(x<jeda)
         ramal[x]=0;
       else
         ramal[x]=at[x-1]+bt[x-1];
      }
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
      }
    }

    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-jeda))*100)/100;

    sessionStorage.setItem('meDES',ME);
    sessionStorage.setItem('maeDES',MAE);
    sessionStorage.setItem('mseDES',MSE);
    sessionStorage.setItem('mapeDES',MAPE);
    return ramal;
  }

  function countDecom(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var jeda =0;
      var ramal=[];

      var ave=[];
      var ctr2=0; //untuk mendapatkan rata-ratanya
      ave[ctr2]=0;
      var hit=0;
      var start=0;
      var x=start;
     while(true){ // average simple
        if(data[x]==null)
          {
          ave[ctr2]=ave[ctr2]/hit;
          ctr2++;
          start++;
          hit=0;
          x=start;
          if(start==data.length-7) break;
          else ave[ctr2]=0;
          continue;
          }
        else{
          ave[ctr2]+=+data[x];
          
        }
        hit++;
        x++;
        if(hit==12){
          ave[ctr2]=ave[ctr2]/12;
          ctr2++;
          ave[ctr2]=0;
          start++;
          hit=0;
          x=start;
          if(start==data.length-4) break;
        }
      }


       var center=[];
       var si=[];
      ctr2=0; //untuk mendapatkan rata-ratanya
      center[ctr2]=0;
      hit=0;
      start=0;
      x=start;
      while(true){ // average centered
        if(ave[x]==null)
          {
          center[ctr2]=center[ctr2]/hit;
          si[ctr2]=+data[start+6]/center[ctr2];
          ctr2++;
          start++;
          hit=0;
          x=start;
          if(start==ave.length-1) break;
          else center[ctr2]=0;
          continue;
          }
        else{
          center[ctr2]+=ave[x];
        }
        hit++;
        x++;
        if(hit==6){
          center[ctr2]=center[ctr2]/6;
          si[ctr2]=+data[x]/center[ctr2];
          ctr2++;
          center[ctr2]=0;
          start++;
          hit=0;
          x=start;
        }
      }
      // app.console.log('');
      // app.console.log(si);

      var cek=[];
      var ctr=0;//untuk variabel bulan ke berapa
      var ctr3=0;//untuk bulannya
      hit=0;
      var unadjusted=[];
      var sumunad=0;
      unadjusted[ctr3]=0;
      x=0;
      y=x;
      while(true){
       
          if(si[x]!=null){
            unadjusted[ctr3]+=si[x];
            hit++;
            x+=12;
          }
          else{
            y++;
            x=y;
            unadjusted[ctr3]=unadjusted[ctr3]/hit;
            sumunad+=unadjusted[ctr3];
            hit=0;
            ctr3++;
            if(y==12) break;
            else unadjusted[ctr3]=0;
          }

      }

      x=6;
      var adjustedorder=[];
      ctr=0;
      for(var y=0;y<unadjusted.length;y++){
        adjustedorder[ctr]=unadjusted[x]*12/sumunad;
        ctr++;
        x++;
        if(x==12) x=0;
      }

      var deseason=[];
      x=0;
      for(var y=0;y<data.length;y++){
        deseason[y]=Math.round(+data[y]/adjustedorder[x]);
        x++;
        if(x==12){
          x=0;
        }
      }

       var t=0;
        var yt=0;
        var tyt=0;
        var t2=0;

        for(var x=0;x< deseason.length;x++){
          t+=(x+1);
          yt+=parseInt(deseason[x]);
          tyt+=(parseInt(deseason[x])*(x+1));
          t2+=(Math.pow((x+1),2));
        }

        
        var b=((bulan.length*tyt)-(yt*t))/((bulan.length*t2)-Math.pow(t,2));
        var a=(yt-(b*t))/bulan.length;
     
  hit=0;
  var trend=[];
    for(var x=0;x< bulan.length;x++){
      var peramalan=Math.round(a+(b*(x+1)));
      trend[x]=peramalan;
      ramal[x]=Math.round(trend[x]*adjustedorder[hit]);
      hit++;
      if(hit==12){
        hit=0;
      }
      
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(+data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(+data[x])-ramal[x]);
        //app.console.log(data[x]+' - '+ramal[x]+' = '+(parseInt(+data[x])-ramal[x]));
        kesalahanA+=Math.abs((parseInt(+data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(+data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(+data[x])-ramal[x])/parseInt(+data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(+data[x])-ramal[x]))/parseInt(+data[x])*100);
        
      }
    }

    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-jeda))*100)/100;

    sessionStorage.setItem('meDEC',ME);
    sessionStorage.setItem('maeDEC',MAE);
    sessionStorage.setItem('mseDEC',MSE);
    sessionStorage.setItem('mapeDEC',MAPE);
    return ramal;
  }

  function countLinier(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var jeda =0;
    var ramal=[];
   
    var t=0;
    var yt=0;
    var tyt=0;
    var t2=0;

    for(var x=0;x< bulan.length;x++){
      t+=(x+1);
      yt+=parseInt(data[x]);
      tyt+=(parseInt(data[x])*(x+1));
      t2+=(Math.pow((x+1),2));
    }

    
    var b=Math.round(((bulan.length*tyt)-(yt*t))/((bulan.length*t2)-Math.pow(t,2)));
    var a=(yt-(b*t))/bulan.length;
    
    for(var x=0;x< bulan.length;x++){
      var peramalan=Math.round(a+(b*(x+1)));
      ramal[x]=peramalan;

    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;

    for(var x=0;x< bulan.length;x++){
     
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
    }
    
    var ME=Math.round((Math.abs(kesalahan)/(bulan.length-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(bulan.length-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(bulan.length-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(bulan.length-jeda))*100)/100;

    sessionStorage.setItem('meLIN',ME);
    sessionStorage.setItem('maeLIN',MAE);
    sessionStorage.setItem('mseLIN',MSE);
    sessionStorage.setItem('mapeLIN',MAPE);
    return ramal;
  }